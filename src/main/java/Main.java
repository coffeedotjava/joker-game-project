import java.util.Scanner;

public class Main {
    public static void main(String[] args){

        System.out.println("Welcome to Pisti Game");
        System.out.println("Enter an number to start game");
        System.out.println("1 for PC vs PC game");
        System.out.println("2 for User vs User game");
        System.out.println("3 for Pc vs User game");


        Scanner in = new Scanner(System.in);
        int num = in.nextInt();
        while(num < 1 || num > 3){
            System.out.println("Enter an valid input : (1 or 2 or 3)");
            num = in.nextInt();
        }

        Game game = new Game();
        /*
         There are 3 modes of game
         Each ones usages as below
          */
        switch (num){
            case 1 :
                game.initializeGame(num);
                game.startGame();
                break;

            case 2 :
                game.initializeGame(num);
                if(game.connection())
                    game.startGame();
                else {
                    System.err.println("Connection Error. Game Terminating");
                    System.exit(1);
                }
            case 3 :
                game.initializeGame(num);
                game.startGame();
        }
    }
}
