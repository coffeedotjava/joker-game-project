import Card.Card;
import Card.Impl.Club;
import Card.Impl.Diamond;
import Card.Impl.Heart;
import Card.Impl.Spade;
import Player.Impl.Computer;
import Player.Impl.Human;
import Player.Player;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.*;

public class Game {
    private ServerSocket server;
    private Socket client;

    private PrintWriter clientOut;
    private BufferedReader clientIn;
    private BufferedReader clientStdIn;
    private String fromServer = "", toServer;

    private Socket clientSocket;
    private PrintWriter serverOout;
    private BufferedReader serverIn;
    private BufferedReader serverStdIn;
    private String fromClient = "", toClient = "";



    private int portNumber;
    private String IPaddress = "localhost";

    /**
     * Coefficient of card points
     */
    private final static int joker = 20;
    private final static int normal = 10;
    private final static int club2 = 2;
    private final static int diamond10 = 3;
    private final static int as = 1;
    private final static int j = 1;

    /**
     * Counts that how much move done
     */
    private int move = 0;
    private boolean isEnd = false;

    private Player p1;
    private Player p2;
    private Player currentPlayer;
    private Player pilePlayer;

    /**
     * Cards in the deck
     */
    private Stack<Card> deck;
    /**
     * Used for playing deck
     */
    private Stack<Integer> intDeck;

    /**
     * Cards in the table
     */
    private Stack<Card> table;


    final int cnonHand = 4;
    final int typeNumber = 4;
    final int cnSingleType = 13;
    final int cardNumber = cnSingleType * typeNumber;

    private List<Card> club;
    private List<Card> diamond;
    private List<Card> heart;
    private List<Card> spade;

    /**
     * No parameter constructor. Initializes objects
     */
    public Game() {
        club = new ArrayList<Card>(cnSingleType);
        diamond = new ArrayList<Card>(cnSingleType);
        heart = new ArrayList<Card>(cnSingleType);
        spade = new ArrayList<Card>(cnSingleType);

        deck = new Stack();
        intDeck = new Stack<Integer>();
        table = new Stack();

    }

    /**
     * Starts the game when everything is ready
     * This is an GamePlay
     */
    public void startGame(){
        Card hold;
        while (!(isEnd && deck.empty() && p1.isFree() && p2.isFree())){
            fromClient = null;
            System.out.println(printState(false));
            if(otherPlayer().getMultiplayer()){
                List<Card> print = new ArrayList<Card>();
                for (int i = table.size()-1; i >= 0 ; i--) {
                    print.add(table.elementAt(i));
                }
                serverOout.println("Pile Cards " + print.toString());
            }

            if(currentPlayer.getMultiplayer()){
                try {
                    fromClient = serverIn.readLine();
                    hold = currentPlayer.discard(fromClient);
                    while(hold == null){
                        serverOout.println("Wrong input");
                        fromClient = serverIn.readLine();
                        hold = currentPlayer.discard(fromClient);
                    }
                }catch (IOException e){
                    hold = null;
                    e.printStackTrace();
                }

            }
            else
                hold = currentPlayer.discard(fromClient);

            while (hold == null){
                System.out.println("Enter valid input like: (card type)|(card value)");
                hold = currentPlayer.discard("");
            }
            p1.brain(hold);
            p2.brain(hold);
            try {
                if(hold.getValence().equals(table.peek().getValence()) || hold.getValence() == "J") {
                    addPoints(hold);
                    table.push(hold);
                    currentPlayer.add(table);
                    table.removeAllElements();
                    System.out.println(printState(true));
                    changePlayer();
                }else {
                    table.push(hold);
                    changePlayer();
                }
            }catch (EmptyStackException e){
                table.push(hold);
                changePlayer();
            }
            checkCN();
        }
        if(otherPlayer().getMultiplayer())
            serverOout.println(printState(false));

        System.out.println(printState(false));
        try {
            server.close();
            client.close();
        }catch (IOException e){
            System.err.println("Error while closing sockets");
        }
    }

    /**
     * Makes connection server and client
     * @return true if connection successful, false otherwise
     */
    public boolean connection(){
        System.out.println("Enter an port number");
        Scanner in = new Scanner(System.in);
        portNumber = in.nextInt();
        try {
            server = new ServerSocket(portNumber);
            serverConfig();
        }
        catch (IOException e){
            try {
                client = new Socket(InetAddress.getByName(IPaddress), portNumber);
                clientConfig();
                clientPlay();
            }catch (UnknownHostException ex) {
                return false;
            }catch (IOException ex){
                return false;
            }
        }
        return true;
    }

    /**
     * Configuration for client
     * Creates Streams and makes game ready to play
     */
    private boolean clientConfig(){
        System.out.println("I'm a client");

        try {
            clientOut = new PrintWriter(client.getOutputStream(), true);
            clientIn = new BufferedReader(new InputStreamReader(client.getInputStream()));
            clientStdIn = new BufferedReader(new InputStreamReader(System.in));
            return true;
        }catch (UnknownHostException ex) {
            return false;
        }catch (IOException ex){
            return false;
        }
    }

    /**
     * Game Play for client
     * Sends cards to the server via socket connection
     */
    private void clientPlay(){
        try {
            while (fromServer.indexOf("Game Finished") == -1){
                fromServer = clientIn.readLine();
                if(fromServer.contains("Wrong input")){
                    System.out.println("Enter valid input like: (card type)|(card value)");
                }
                else
                    System.out.println(fromServer);
                toServer = clientStdIn.readLine();
/*                if(card == null){
                    System.out.println("Enter valid input like: (card type)|(card value)");
                    card = currentPlayer.discard("");
                }
                toServer = card.toString();*/
                clientOut.println(toServer);
            }
        }catch (IOException e){

        }
    }
    /**
     * Configuration for server
     * Creates Streams and makes game ready to play
     */
    private void serverConfig(){
        System.out.println("I'm a server");

        try {
            clientSocket = server.accept();
            serverOout = new PrintWriter(clientSocket.getOutputStream(), true);
            serverIn = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        } catch (IOException e) {

        }
    }

    /**
     * Initializes and makes table ready to game
     * @param mode running mode of game. User, PC etc.
     * @return false if mode is not valid. otherwise true
     */
    public boolean initializeGame(int mode){
        if(mode > 3 || mode < 1){
            return false;
        }
        if(mode == 1){
            p1 = new Computer(1, table);
            p2 = new Computer(2, table);
        }
        else if(mode == 2){
            p1 = new Human(1);
            p2 = new Human(2);
            p2.setMultiplayer(true);
        }
        else if(mode == 3){
            p1 = new Human(1);
            p2 = new Computer(2, table);
        }
        currentPlayer = p1;
        firstInit();
        playing();
        deal(true);

        return true;
    }

    /**
     * Assigns values to the cards
     */
    private void firstInit(){
        club.add(new Club("A"));
        diamond.add(new Diamond("A"));
        heart.add(new Heart("A"));
        spade.add(new Spade("A"));

        for (Integer i = 2; i < 11; i++) {
            club.add(new Club(i.toString()));
            diamond.add(new Diamond(i.toString()));
            heart.add(new Heart(i.toString()));
            spade.add(new Spade(i.toString()));
        }
        club.add(new Club("J"));
        diamond.add(new Diamond("J"));
        heart.add(new Heart("J"));
        spade.add(new Spade("J"));

        club.add(new Club("Q"));
        diamond.add(new Diamond("Q"));
        heart.add(new Heart("Q"));
        spade.add(new Spade("Q"));

        club.add(new Club("K"));
        diamond.add(new Diamond("K"));
        heart.add(new Heart("K"));
        spade.add(new Spade("K"));
    }

    /**
     * Riffles cards
     */
    private void playing(){
        for (int i = 0; i < cardNumber; i++) {
            intDeck.push(i);
        }
        Collections.shuffle(intDeck);
        for (int i = 0; i < cardNumber; i++) {
            int n = intDeck.pop();
            if(n < cnSingleType){
                deck.push(club.get(n%cnSingleType));
            }
            else if(n < cnSingleType*2){
                deck.push(diamond.get(n%cnSingleType));
            }
            else if(n < cnSingleType*3){
                deck.push(heart.get(n%cnSingleType));
            }
            else{
                deck.push(spade.get(n%cnSingleType));
            }
        }
    }

    /**
     * Deals playing cards to players and table
     * @param flag true for first time, otherwise true
     */
    private void deal(boolean flag){
        try {
            if(flag){
                for (int i = 0; i < cnonHand; i++) {
                    table.push(deck.pop());
                    p1.brain(table.peek());
                    p2.brain(table.peek());
                }
            }
            for (int i = 0; i < cnonHand; i++) {
                p1.take(deck.pop());
            }
            for (int i = 0; i < cnonHand; i++) {
                p2.take(deck.pop());
            }

        }catch (EmptyStackException e){
            isEnd = true;
        }
    }

    /**
     * Changes current player
     */
    private void changePlayer(){
        if(currentPlayer == p1){
            currentPlayer = p2;
        }else{
            currentPlayer = p1;
        }
    }

    private Player otherPlayer(){
        if(currentPlayer == p1){
            return p2;
        }else{
            return p1;
        }
    }

    /**
     * Checks that does players have cards on hands
     */
    private void checkCN(){
        if(p1.isFree() && p2.isFree()){
            deal(false);
        }
    }

    /**
     * Adds specific points to the player that gets hand
     */
    private void addPoints(Card hold){
        if(table.peek().getValence().equals("J") && hold.getValence().equals("J")){
            currentPlayer.increasePoint(joker);
        }
        else if(hold.getValence().equals(table.peek().getValence())){
            currentPlayer.increasePoint(normal);
        }
        if(table.toString().contains("C_2")){
            currentPlayer.increasePoint(club2);
        }
        if(table.toString().contains("D_10")){
            currentPlayer.increasePoint(diamond10);
        }
        if(table.toString().contains("C_A")
                || table.toString().contains("D_A")
                || table.toString().contains("H_A")
                || table.toString().contains("S_A") ){
            currentPlayer.increasePoint(as);
        }
        if(table.toString().contains("C_J")
                || table.toString().contains("D_J")
                || table.toString().contains("H_J")
                || table.toString().contains("S_J")){
            currentPlayer.increasePoint(j);
        }
    }

    /**
     * Prints last state of the game
     * @param flag for 'pisti' it is true, else false
     */
    private String printState(boolean flag){
        StringBuilder stringBuilder = new StringBuilder();
        if(!isEnd){
            stringBuilder.append("Round1_Move " + move + "\n");
            if(flag){
                stringBuilder.append(currentPlayer.toString() + "_Get_Cards= " + currentPlayer.frontCard() + "\n");

                pilePlayer = currentPlayer;
            }else{
                List<Card> print = new ArrayList<Card>();
                for (int i = table.size()-1; i >= 0 ; i--) {
                    print.add(table.elementAt(i));
                }
                stringBuilder.append("Pile_cards = " + print.toString() + "\n");
                stringBuilder.append("Player_1_Hand = " + p1.handCard() + "\n");
                stringBuilder.append("Player_2_Hand = " + p2.handCard() + "\n");
            }
            stringBuilder.append("Player_1_Points = " + p1.getPoint() + "\n");
            stringBuilder.append("Player_2_Points = " + p2.getPoint() + "\n");
        }
        else {
            stringBuilder.append("Game Finished" + "\n");
            stringBuilder.append("Assign_Pile_Left_Cards = " + pilePlayer.toString() + "\n");
            stringBuilder.append("Player_1_Bag = " + p1.frontCard() + "\n");
            stringBuilder.append("Player_2_Bag = " + p2.frontCard() + "\n");
            stringBuilder.append("Player_1_Points = " + p1.getPoint() + "\n");
            stringBuilder.append("Player_2_Points = " + p2.getPoint() + "\n");
            if(p1.getPoint() > p2.getPoint()){
                stringBuilder.append("Player_1 Won!" + "\n");
            }else if(p1.getPoint() < p2.getPoint()){
                stringBuilder.append("Player_2 Won!" + "\n");
            }else {
                stringBuilder.append("Draw" + "\n");
            }
        }
        ++move;

        return stringBuilder.toString();
    }

    /**
     * Alternative version of start game
     */
    /*private void alternative(){
        Card hold;
        while (!deck.empty()){
            printState(false);
            hold = currentPlayer.discard();
            while (hold == null){
                System.out.println("Enter good input like: (card type)|(card value)");
                hold = currentPlayer.discard();
            }
            checkCN();
            if(hold.getValence() == table.peek().getValence() || hold.getValence() == "J"){
                if(table.peek().getValence() == "J" && hold.getValence() == "J"){
                    currentPlayer.increasePoint(joker);
                }
                else if(hold == table.peek()){
                    currentPlayer.increasePoint(normal);
                }
                table.push(hold);
                addPoints();
                printState(true);
                currentPlayer.add(table);
                table.removeAllElements();
                changePlayer();
                hold = currentPlayer.discard();

                while (hold == null){
                    System.out.println("Enter good input like: (card type)|(card value)");
                    hold = currentPlayer.discard();
                }
                table.push(hold);
                checkCN();
                changePlayer();
            }else{
                table.push(hold);
                changePlayer();
                printState(false);
                hold = currentPlayer.discard();
                while (hold == null){
                    System.out.println("Enter good input like: (card type)|(card value)");
                    hold = currentPlayer.discard();
                }
                checkCN();
                if((hold.getValence() == table.peek().getValence()) || hold.getValence() == "J"){
                    addPoints();
                    printState(true);
                    table.push(hold);
                    currentPlayer.add(table);
                    table.removeAllElements();
                    changePlayer();
                    hold = currentPlayer.discard();

                    while (hold == null){
                        System.out.println("Enter good input like: (card type)|(card value)");
                        hold = currentPlayer.discard();
                    }
                    table.push(hold);
                    checkCN();
                    changePlayer();
                }
                else{
                    table.push(hold);
                    changePlayer();
                }
            }
        }
    }*/
}
