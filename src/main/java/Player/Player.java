package Player;

import Card.Card;
import java.util.Stack;

public interface Player {

    Card discard(String fromClient);
    void add(Stack<Card> cards);
    void take(Card card);
    boolean isFree();
    int getPoint();
    void increasePoint(int n);
    String handCard();
    String frontCard();
    void brain(Card card);
    void setMultiplayer(boolean b);
    boolean getMultiplayer();
}
