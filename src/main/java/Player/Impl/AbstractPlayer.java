package Player.Impl;

import Card.Card;
import Player.Player;

import java.util.List;
import java.util.Stack;

public abstract class AbstractPlayer implements Player {


    public Card discard(String fromClient) {
        return null;
    }

    /**
     * Adds cards to players pocket
     * @param cards comes from table
     */
    public void add(Stack<Card> cards) {
        while(!cards.empty()){
            front.push(cards.pop());
        }
    }

    /**
     * Gets card when dealing cards one by one
     * @param card comes from deal
     */
    public void take(Card card) {
        hand.add(card);
        brain(card); // also adds brain
    }

    /**
     * Checks that does player has any card
     * @return true if no cards, otherwise false
     */
    public boolean isFree(){
        return hand.isEmpty();
    }

    /**
     * Returns point of player
     * @return Point of player
     */
    public int getPoint(){
        return point;
    }

    /**
     * Increases point of player by param
     * @param n number of points player's get
     */
    public void increasePoint(int n){
        point += n;
    }

    /**
     * Creates string of cards on hand
     * @return String of hand cards
     */
    public String handCard() {
        String string = new String();
        for (Card card: hand) {
            string += card.toString();
            string += " | ";
        }
        return string;
    }
    public void brain(Card card){}

    /**
     * Creates string of cards in front
     * @return String of front cards
     */
    public String frontCard(){
        return front.toString();
    }

    public void setMultiplayer(boolean b) {

    }
    public boolean getMultiplayer(){
        return false;
    }
    @Override
    public String toString() {
        return playerStr;
    }

    /**
     * Cards that got by player
     */
    Stack<Card> front;

    /**
     * Cards that hand by player
     */
    List<Card> hand;

    /**
     * Point of player
     */
    int point;

    /**
     * String of player
     */
    String playerStr;

}
