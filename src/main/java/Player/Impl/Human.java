package Player.Impl;

import Card.Impl.Club;
import Card.Impl.Diamond;
import Card.Impl.Heart;
import Card.Impl.Spade;
import java.util.*;
import Card.Card;

public class Human extends AbstractPlayer {
    public Human(int n) {
        hand = new ArrayList<Card>(4);
        front = new Stack();
        reader = new Scanner(System.in);
        point = 0;
        playerStr = "Player_" + n;
    }

    /**
     * Gets string from client
     * @param fromClient It will be used only multiplayer games
     * @return returns card that choosen by human
     */
    public Card discard( String fromClient) {

        try {
            String cardstr;
            if(fromClient != null){
                cardstr = fromClient;
            }else {
                System.out.print(playerStr + " enter the card that you’d like to play: ");
                cardstr = reader.nextLine();
            }
            Card card = getInput(cardstr);
            for (Card card1:hand) {
                if (card1.getType() == card.getType()
                    && card1.getValence().equals(card.getValence())){
                    return hand.remove(hand.indexOf(card1));
                }
            }
            return null;
        }catch (InputMismatchException e){
            return null;
        }catch (StringIndexOutOfBoundsException e){
            return null;
        }
    }

    /**
     * This method gets card string and creates a valid
     * Card object. If this string is not valid, throws an exception
     * @param card String of a card
     * @return Valid card object
     */
    private Card getInput(String card){

        if((card.charAt(2) == '2' || card.charAt(2) == '3'
                ||  card.charAt(2) == '4' || card.charAt(2) == '5'
                ||  card.charAt(2) == '6' ||  card.charAt(2) == '7'
                ||  card.charAt(2) == '8' ||  card.charAt(2) == '9'
                ||  card.charAt(2) == 'J' ||  card.charAt(2) == 'Q'
                ||  card.charAt(2) == 'K' ||  card.charAt(2) == 'A')
                && card.charAt(1) == '_'){
            if((card.charAt(0) == 'C' || card.charAt(0) == 'c')){
                return new Club(card.substring(2));
            }
            else if((card.charAt(0) == 'D' || card.charAt(0) == 'd')){
                return new Diamond(card.substring(2));
            }
            else if((card.charAt(0) == 'H' || card.charAt(0) == 'h')){
                return new Heart(card.substring(2));
            }
            else if((card.charAt(0) == 'S' || card.charAt(0) == 's')){
                return new Spade(card.substring(2));
            }
            else{
                throw new InputMismatchException();
            }
        }
        else if(card.charAt(2) == '1' && card.charAt(3) == '0' && card.charAt(1) == '_'){
            if((card.charAt(0) == 'C' || card.charAt(0) == 'c')){
                return new Club(card.substring(2).toString());
            }
            else if((card.charAt(0) == 'D' || card.charAt(0) == 'd')){
                return new Diamond(card.substring(2).toString());
            }
            else if((card.charAt(0) == 'H' || card.charAt(0) == 'h')){
                return new Heart(card.substring(2).toString());
            }
            else if((card.charAt(0) == 'S' || card.charAt(0) == 's')){
                return new Spade(card.substring(2).toString());
            }
            else{
                throw new InputMismatchException();
            }
        }
        else{
            throw new InputMismatchException();
        }

    }

    /**
     * Sets is game single or multi player
     * @param multiplayer
     */
    public void setMultiplayer(boolean multiplayer) {
        this.multiplayer = multiplayer;
    }

    /**
     * Returns state of game
     * @return
     */
    public boolean getMultiplayer() {
        return multiplayer;
    }

    private boolean multiplayer = false;
    private Scanner reader;

}
