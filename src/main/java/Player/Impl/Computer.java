package Player.Impl;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;
import java.util.Stack;
import Card.Card;

public class Computer extends AbstractPlayer {


    public Computer(int n, Stack<Card> table) {
        hand = new ArrayList<Card>(4);
        mind = new ArrayList<Card>(52);
        front = new Stack();
        point = 0;
        playerStr = "Player_" + n;
        mind = new ArrayList<Card>(52);
        this.table = table;
    }

    /**
     * Adds param to the computer's memory
     * @param card seen in the game in table or computer's hand
     */
    public void brain(Card card){
        if(!mind.contains(card))
            mind.add(card);
    }

    /**
     * This method chooses which card will be played by computer
     * It looks that table, own brain and own hand
     * Makes decision to play card
     * @param fromClient this will be used if this is an multiplayer game
     * @return Choosen card
     */
    public Card discard(String fromClient) {
        int index = 0;
        int value[] = {0,0,0,0};

        for (int i = 0; i < hand.size(); i++) {
            try {
                if(table.peek().getValence().equals(hand.get(i).getValence())){
                    value[i] += 10;
                    if(hand.get(i).getValence().equals("J")){
                        value[i] += 10;
                    }
                }
                if (table.size() > 7 && hand.get(i).getValence().contains("J")) {
                    value[i] += 2;
                }
                for (int j = 0; j < mind.size(); j++) {
                    if (mind.get(j).getValence().equals(hand.get(i).getValence())) {
                        ++value[i];
                    }
                }
            }catch (EmptyStackException e){

            }

        }
        for (int i = 0; i < hand.size() - 1; i++) {
            if(value[i+1] > value[i]){
                index = i+1;
            }
        }
        System.out.println(playerStr + " plays: " + hand.get(index) + "\n");
        return hand.remove(index);
    }

    /**
     * Referance of game table
     * Computer needs it when making decision
     */
    private Stack<Card> table;

    /**
     * Memory of computer
     * Computer needs it when making decision
     */
    private List<Card> mind;
}
