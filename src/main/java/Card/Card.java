package Card;

public interface Card {
    String getValence();
    String getType();
}
