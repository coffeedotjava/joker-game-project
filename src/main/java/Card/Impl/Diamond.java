package Card.Impl;

import Card.Card;

public class Diamond implements Card{
    public Diamond(String valence) {
        this.valence = valence;
    }

    public String getValence() {
        return valence;
    }

    public String getType() {
        return type;
    }
    @Override
    public String toString() {
        return getType() + "_" + valence;
    }

    /**
     * Value of Card
     */
    private String valence;

    /**
     * Type of Card
     */
    private final String type = "D";
}
