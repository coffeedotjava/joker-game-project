package Card.Impl;

import Card.Card;

public class Club implements Card {
    public Club(String valence) {
        this.valence = valence;
    }

    public String getValence() {
        return valence;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return getType() + "_" + valence;
    }

    /**
     * Value of Card
     */
    private final String valence;

    /**
     * Type of Card
     */
    private final String type = "C";
}
